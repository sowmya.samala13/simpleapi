def is_ODD(numeric_input):
	"""Verifies whether the input parameter 'numeric_input' is a One-Digit-Difference number

	Args:
		numeric_input: The input parameter to verify if it is "ODD" number

	Returns:
		bool: True if input is "ODD" number, False otherwise.

	"""

	# checking whether the input is a numeric/integer type
	if ((not (type(numeric_input) == int)) or (not isinstance(numeric_input, int)) or (numeric_input < 0)):
		return False

	# checking whether the input is of the "ODD" type
	numeric_input_as_string = str(numeric_input)
	input_length = len(numeric_input_as_string)
	for first_index in range(input_length-1):
		absolute_digit_diff = abs(int(numeric_input_as_string[first_index]) - int(numeric_input_as_string[first_index + 1]))
		if (absolute_digit_diff != 1):
			return False

	return True
numeric_input=int(input())
print(is_ODD(numeric_input))


def is_PRIME(numeric_input):
	"""Verifies whether the input parameter 'numeric_input' 

	Args:
		numeric_input: The input parameter to verify if it is "PRIME" number

	Returns:
		bool: True if input is "PRIME" number, False otherwise.

	"""
if numeric_input >1: 
    for i in range(2,int(numeric_input/2)+1):
        if(numeric_input%i) == 0:
            return False
else:
    return False  
num = int (input())        
print(is_PRIME(num))


