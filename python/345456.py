def check_oddness(num):
    diff_list=[abs(int(num[i])-int(num[i+1])) for i in range(len(num)-1)]
    #print(diff_list)
    if list(set(diff_list))==[1]:
        return True
    return False

def is_prime(num):
    if num>1:
        for i in range(2,int(num/2)+1):
            if(num%i==0):
                return False
        else:
            return True
    else:
        return False
        
def count_odd_diff_digit_numbers():
    count=0
    for i in range(10,10000000):
        is_odd=check_oddness(str(i))
        if is_odd and is_prime(i):
            count=count+1
    print(count)
print(check_oddness("345456"),is_prime(345456))
count_odd_diff_digit_numbers()