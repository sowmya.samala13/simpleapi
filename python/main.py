#opening a file in current directory
"""f = open("message.txt")"""

#reading files
"""f = open("message.txt","r")
content = f.read(6)
print(content)
more_content = f.read(10)
print(more_content)
f.close()"""

#exception handling while working with files
"""try:
    f = open("message.txt","r")
    content = f.read(6)
    print(content)
    more_content = f.read(10)
    print(more_content)
    f.close()
finally:
    f.close() """   
     
#writing to file
"""with open("python.txt","w") as f:
    f.write("python is easy\n")
    f.write("i love python")"""

#appending to file
"""with open("python.txt","a") as f:
    f.write("\npython is simple")"""

#write lines method writes the items of a list to the file
"""with open("javascript.txt","w") as f:
    lines = ["js is awesome","\njs is my favorite language"]
    f.writelines(lines)"""

# deleting a file
f = open("message.txt",)   
f.seek()
f.truncate()
f.close()

#reading a single line
"""f = open("python.txt","r")
f.readline()
f.close()"""

#writing to a file
"""f = open('message.txt', "w")
f.writelines("happy")
f.close()"""
 
# Using readlines()
"""f = open('message.txt', "r")
Lines = f.readlines()"""

#readlines
"""with open("python.txt","r") as f:
    file = f.readlines()
    print(*file)"""


