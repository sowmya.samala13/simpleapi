#sudoku puzzle game solving itself when input is given
def solve(unsolve_puzz):
    """Returns the boolean value

    Parameters:
        unsolved_puzzle(list): unsolved puzzle

    Returns:
        Boolean True if it is solved Otherwise retuns False
    """
    find = find_empty(unsolve_puzz)
    if not find:
        return True
    else:
        row, col = find

    for i in range(1,10):
        if valid(unsolve_puzz, i, (row, col)):
            unsolve_puzz[row][col] = i

            if solve(unsolve_puzz):
                return True

            unsolve_puzz[row][col] = 0

    return False


def valid(solve_puzz, num, pos):
    """Returns whether the number is suitable or not

            Parameters:
                solve_puzz(list): unsolved puzzle
                num(int): integer
                pos(int): row and column values

            Returns:
                Return Boolean True if the Number is 
                    suitable at Pos Position Otherwise Returns False
    """
    #To check whether the same number is present in row or not
    for i in range(len(solve_puzz[0])):
        if solve_puzz[pos[0]][i] == num and pos[1] != i:
            return False

    #To Check whether the same number is present in column or not
    for i in range(len(solve_puzz)):
        if solve_puzz[i][pos[1]] == num and pos[0] != i:
            return False

    #To Check whether the same number is present in box or not
    box_x = pos[1] // 3
    box_y = pos[0] // 3

    for i in range(box_y*3, box_y*3 + 3):
        for j in range(box_x * 3, box_x*3 + 3):
            if solve_puzz[i][j] == num and (i,j) != pos:
                return False

    return True


def print_board(solve_puzz):
    """prints the sovlved puzzle.

        Parameters:
           solve_puzzle(list) : solve puzzle
    """
    for i in range(len(solve_puzz)):
        if i % 3 == 0 and i != 0:
            print("- - - - - - - - - - - - - ")

        for j in range(len(solve_puzz[0])):
            if j % 3 == 0 and j != 0:
                print(" | ", end="")

            if j == 8:
                print(solve_puzz[i][j])
            else:
                print(str(solve_puzz[i][j]) + " ", end="")


def find_empty(empty_row_column):
    """Returns row and column positions which are zero values.

        Parameters:
            empty_row_column (list): A list which contains the puzzle values

        Returns:
            i,j: Integer of rows position and coloumn position of it is zero
                  otherwise returns none
    """
    #when there is zero it will treat as empty place 
    for i in range(len(empty_row_column)):
        #After finding the empty place it return row and column postion number
        for j in range(len(empty_row_column[0])):
            if empty_row_column[i][j] == 0:
                return (i, j)  # row, col

    return None

