import random
class omicron:
    """This function is used to make the possible outcomes of the virus in the given range of the
        input: number of viruses by rapid_antigens

    """

    def init(self):
        pass   
    def rapid_anitgens(self,input_number):
        antigens=[]
        for i in range(input_number):
            if random.random() <= 0.7:
                antigens.append(True)
            else:
                antigens.append(False)#antigens'
        a=antigens.count(True)
        b=antigens.count(False)
        return a,b
#it checks whether the test is positive or not in the given list of viruses        
    
    def rtpcr_test(self,input_number):
        rtpcr=[]   
        for i in range(input_number):
            if random.random() <= 0.95:
                rtpcr.append(True)
            else:
                rtpcr.append(False)
        a_1=rtpcr.count(True)
        b_1=input_number-a_1
        return a_1,b_1