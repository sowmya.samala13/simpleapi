class innominds:
    def _init_(self,n):
        pass
    def fib(n):
        print("0",end=" ")
        a=1
        b=1
        c=0
        for i in range(n):
            print(a,end=" ")
            c=a+b
            a=b
            b=c

    def prime(n):
        for i in range(2,n+1):
            count=0
            for j in range(1,i):
                if(i%j==0):
                    count=count+1
            if count<=1:
                print(i,end=" ")
    def ascending(n):
        l=list(map(int,input().split()))[:n]
        l.sort(reverse=0)
        print(l)
    def decending(n):
        l=list(map(int,input().split()))[:n]
        l.sort(reverse=1)
        print(l)
n=int(input())
obj1=innominds
obj1.fib(n)
obj1.prime(n)
obj1.ascending(n)
obj1.decending(n)