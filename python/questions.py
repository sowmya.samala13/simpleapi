#TASK-1
#count of digits present in the username.
user_name=input()
count = 0
for x in user_name:
    if x.isdigit():
       count = count+1
print(count)

#TASK-2
#packet decoder.
string=input()
x,l,data=string[:8],string[8:16],string[16:]
x_int=int(x,2)
length=int(l,2)
string=""
for i in range(0,len(data),8):
    b=data[i:i+8]
    b_int=int(b,2)
    xor1=b_int^x_int
    string=string+chr(xor1)
if len(string)==length:
    print(string)
else:
    print("Invalid Packet")

#TASK-3
#Sum of letters.
def fibonaci(x):
    if x<0:
        return False
    elif x==0:
        return 0
    elif x==1 or x==2:
        return 1
    else:
        return fibonaci(x-1)+fibonaci(x-2)
letter=input()
sum=0
for i in letter.upper():
    a=fibonaci(ord(i)-65)
    sum+=a
print(sum)   

#TASK-4
#Todays Apparel.
n=int(input())
list_elements=list(map(int,input().split(" ")))[:n]
l=list( filter( lambda x: True if x>=0 else False,list_elements)) 
print(len(l))


#TASK-5
saleCount=int(input("Enter saleCount"))
if saleCount>=30 and saleCount<=50:
    print("D")
elif saleCount>=51 and saleCount<=60:
    print("C")
elif saleCount>=61 and saleCount<=80:  
    print("B")
elif saleCount>=81 and saleCount<=100:
    print("A")
else:
    print("invalid")  




    
    
    
