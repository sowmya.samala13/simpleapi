# Create your own database using the template below

import json
import os
# C R U D

def create_table(table_name):
  # create a new .txt using table_name
  # True if table created, False if fail
  with open(table_name,"r") as fp:
      fp.write(json.dumps("table_name"))
      return True #it returns true when table is created
     

    
def read_table_record(table_name):
  # read the table with "table_name".txt and return it as a PYTHON list
  # inside the text file, of course its all saved in JSON
  f=open(table_name,"r+") #Opens a file for reading and writing
  a=f.read()
  data_bases=json.loads(a)
  f.close()
  return data_bases #retuns the databases

def read_table_record(table_name, condition_object): # { id, name, dept } // {"salary": 678}
  # read the table with "table_name".txt and return only those items which match 
  # the condition object, or return empty list
    f=open(table_name,"r+") #Opens a file for reading and writing
    a=f.read()
    for i in a:
        if condition_object==1:
            print(i)
        f.close()   #closing the file     



def write_table_record(table_name, data_object):
  # to the table in text file "table_name".txt add data_object
  # make sure you are doing some basic checks, this is adding data (id)
  # inform the user what happened
  with open(table_name,'r+') as file: #Opens a file for reading and writing
        # First we load existing data into a dict.
        file_data = json.load(file)
        # Join new_data with file_data inside emp_details.
        file_data["employees"].append(data_object)
        # Sets file's current position at offset.
        file.seek(0)
        # convert back to json.
        json.dump(file_data, file, indent = 4)
        print(file_data) # prints the file_data        


def delete_table_record(table_name,name,id,designation):
  # find data in the table with {id} and delete it
  # also return it to the user
    f=open(table_name,"r+") #Opens a file for reading and writing
    a=f.read() 
    for i in a:
        if id==i:
            print(i)
            del i
            f.close() #closes the file

def delete_table(table_name):
  # delete the table with "table_name".txt
  # inform the user
  os.remove(table_name) #removes the table "table_name"
  return
# for udpate_table_record, delete_table_record, read_table_record -> condition object
