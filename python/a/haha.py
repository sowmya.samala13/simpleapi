#TASK-1:
#Amstrong number
amstrong_number=input()
output=0
for x in amstrong_number:
 output+=int(x)**len(amstrong_number)
if amstrong_number==str(output):
 print(1)
else:
 print(output)

#TASK-2: 
# Function to find average
# of ASCII value of chars
def averageValue(s):
    sum_char = 0
    # loop to sum the ascii
    # value of chars
    for i in range(len(s)):
        sum_char += ord(s[i])
    # Returning average of chars
    return sum_char / len(s)
s = input("Enter a string in lowercase:")
print(averageValue(s))

#TASK-3:
#write an algorithm to find the group for a given book sale count
saleCount=int(input("Enter saleCount:"))
if saleCount>=30 and saleCount<=50:
    print("D")
elif saleCount>=51 and saleCount<=60:
    print("C")
elif saleCount>=61 and saleCount<=80:  
    print("B")
elif saleCount>=81 and saleCount<=100:
    print("A")
else:
    print("invalid")

#TASK-4:
#estimate the cost painting a property
n_interior = int(input("Enter no of interior walls \n"))
n_exterior = int(input("Enter no of exterior walls \n"))
sa_interior = 0
sa_exterior = 0
for i in range(0, n_interior):
    n = float(input("Enter interior walls area \n"))
    sa_interior += n
for i in range(0, n_exterior):
    n2 = float(input("Enter exterior walls area \n"))
    sa_exterior += n2
cost_interior = sa_interior*18
cost_exterior = sa_exterior*12
total_cost_interior_exterior = cost_exterior+cost_interior
print(total_cost_interior_exterior)    

#TASK-5:
#Ration shop
val=list(map(int,input("Enter values:").split()))
mint=val[0]
output_sum=mint
for x in range(val[1]-1):
    b=output_sum-1
    output_sum+=b
print(output_sum)

#TASK-6:
#Function to count patterns
def countpattern(str):
    # Variable to store the last character
    last = str[0]
    i = 1; counter = 0
    while (i < len(str)): 
        # We found 0 and last character was '1',
        # state change
        if (str[i] == '0' and last == '1'):
            while (str[i] == '0'):
                i += 1  
                # After the stream of 0's, we got a '1',
                # counter incremented
                if (str[i] == '1'):
                    counter += 1
        # Last character stored
        last = str[i]
        i += 1
    return counter 
# Driver Code
string = input("Enter a value:")
output= countpattern(string)
print (output)

#TASK-7:
#remove all the elements from the array except perfect squares of any integer and print the updated Array.
array=int(input())
l=list(map(int,input().split()))
output=[]
for i in l:
 x=int(i**0.5) 
 if x*x==i:
    output.append(i)
print(output) 

#TASK-8:
#payment calculation
numhours=9
regular=8
regpay=10
overpay=15
overtime=numhours-regular
if overtime>0:
 pay_amount=(regular*regpay)+(overtime*overpay)
else:
 Pay_amount=numhours * regular
print("The amount to be paid is ",pay_amount)

#TASK-8:
#highest selling products
products=int(input())
list_1=[]
list_2=[]
for j in range(products):
    x,y=input().split()
    list_1.append(x)
    list_2.append(y)
a=list_2.copy()
list_2.sort(reverse=True)
b=list_1[a.index(list_2[0])]
c=list_1[a.index(list_2[1])]
print(b,list_2[0])
print(c,list_2[1])




