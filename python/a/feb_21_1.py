import requests
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor, as_completed
from time import perf_counter
from PIL import Image


def download_file(url, filename):
    req = requests.get(url, stream=True)
    with open(filename, "wb") as file:
        for chunk in req.iter_content(chunk_size=8*1024):
            file.write(chunk)
        file.resize()
    print(f"{filename} downloaded successfully from {url}")


def get_image_urls(url):
    req = requests.get(
        "https://bloggerspassion.com/travel-blogs-in-india/")
    html_doc = req.text
    soup = BeautifulSoup(html_doc, 'html.parser')
    image_urls = []
    for img in soup.find_all("img"):
        try:
            if img["class"][0].startswith("wp-image-"):                
                img_url = img["src"]
                image_urls.append(img_url)

        except KeyError as exc:
            pass
    return image_urls


def main():
    image_urls = get_image_urls(
        "https://bloggerspassion.com/travel-blogs-in-india/")

    print(image_urls)

    with ThreadPoolExecutor(max_workers=4) as executor:
        futures = {executor.submit(
            download_file, image_url, image_url.split("/")[-1]): image_url for image_url in image_urls}
        for future in as_completed(futures):
            url = futures[future]
            print(f"image downloaded successfully from {url}")

    # for url in image_urls:
    # download_file(url, url.split("/")[-1])
if __name__ == "_main_":
    start_time = perf_counter()
    main()
    end_time = perf_counter()
    time_lapsed = (end_time - start_time)
    print(f"finished executing in {time_lapsed}")