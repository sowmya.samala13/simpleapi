import os
import PIL
from PIL import Image  
from PIL import Image, ImageFilter
import requests
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor, as_completed
from time import perf_counter

def download_file(url, filename):
    req = requests.get(url, stream=True)
    with open(filename, "wb") as file:
        for chunk in req.iter_content(chunk_size=8*1024):
            file.write(chunk)
    print(f"{filename} downloaded successfully from {url}")

def get_image_urls(url):
    req = requests.get(
        "https://unsplash.com/s/photos/red-flower")
    html_doc = req.text
    soup = BeautifulSoup(html_doc, 'html.parser')
    image_urls = []
    for img in soup.find_all("img"):
        try:        
                img_url = img['srcset']
                image_urls.append(img_url)
        except KeyError as exc:
            print(exc)
    return image_urls

def blur_resize():
    fld = r'C:\Users\ssamala\Desktop\python\a\trainer'
    print(os.listdir(fld))
    for file in os.listdir(fld):
        if file.endswith(('.jpg', '.jpeg')):
            f_img = fld+"\\"+file
            img=Image.open((f_img))
            img=img.resize((300,300))
            img.save(f_img)
          
def main():
    image_urls = get_image_urls(
        "https://unsplash.com/s/photos/red-flower")
    print(image_urls)
    with ThreadPoolExecutor(max_workers=4) as executor:
        futures = {executor.submit(
            download_file, image_url, image_url.split("/")[-1]): image_url for image_url in image_urls}
        for future in as_completed(futures):
            url = futures[future]
            print(f"image downloaded successfully from {url}")
            
if __name__ == "__main__":
    start_time = perf_counter()
    main()
    end_time = perf_counter()
    time_lapsed = (end_time - start_time)
    print(f"finished executing in {time_lapsed}")