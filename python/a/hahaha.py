
"""#TASK-1:encoder to decoder and decoder to encoder
encoder_mess = input("Enter encoded message")
decoder_mess = ""
for each_char in encoder_mess:
    if each_char.isalpha():
        decoder_mess+=chr(ord(each_char)+5)
    else:
        decoder_mess+=each_char
print(decoder_mess)   
decoder_mess = input("Enter decoded message")
encoder_mess = ""
for each_char in decoder_mess:
    if each_char.isalpha():
        encoder_mess+=chr(ord(each_char)-5)
    else:
        encoder_mess+=each_char
print(encoder_mess) """       


"""
#TASK-3:Bank Compare
bank = []
principal = int(input())
year = int(input())
for i in range(0, 2): # 2 Banks
    installments = int(input())
    sum = 0
    for i in range(0, installments):
        time, roi = [float(i) for i in input().split()]
        square = pow((1+roi), time*12)
        emi = (principal*(roi)/(1-1/square))
        sum = sum + emi
    bank.append(sum)
if bank[0] and bank[1]:
    print(quot;Bank A&quot)
else:
    print("Bank B",quot)



#TASK-2:
# Function to find the majority element present in a given list
def findMajorityElement(nums):
    # create an empty `HashMap`
    d = {}
    # store each element's frequency in a dictionary
    for i in nums:
        d[i] = d.get(i, 0) + 1
    # return the element if its count is more than `n/2`
    for key, value in d.items():
        if value > len(nums) / 2:
            return key
    # no majority element is present
    return -1
# assumption: valid input (majority element is present)
nums = [1, 8, 7, 4, 1, 2, 2, 2, 2, 2, 2]
 
result = findMajorityElement(nums)
if result != -1:
    print('The majority element is', result)
else:
    print('The majority element doesn\'t exist')"""

#TASK-4:
n_int = int(input("enter no of interior walls \n"))
n_ext = int(input("Enter no of exterior walls \n"))
sa_int = 0
sa_ext = 0
for i in range(0, n_int):
    n = float(input("Enter interior walls area \n"))
    sa_int += n
for i in range(0, n_ext):
    n2 = float(input("Enter exterior walls area \n"))
    sa_ext += n2
cost_int = sa_int*18
cost_ext = sa_ext*12
total_cost = cost_ext+cost_int
print(total_cost)
