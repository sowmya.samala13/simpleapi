import logging
logging.basicConfig(filename="newfile.log",format='%(asctime)s %(message)s',filemode='w')
#object creation
logger=logging.getLogger()
logger.setLevel(logging.DEBUG)

#ZeroDivisionError
a=7
b=0
try:
    c=a/b
    logger.info(c)
except Exception as err:
    logger.debug(err)
else:
    logger.error("No exception in risky code")


#TypeError
a = 7
b='sow'
try:
    logger.error('{}{} sum:{}'.format(a,b,a+b))
except Exception as err:
    logger.error(err)
else:
    logger.error("No exception in risky code")


#NameError
names=["sowmya","bhavya","anjali","bannu","nani","lalli"]
try:
    logger.error(names)
except Exception as err:
    logger.error(err)
else:
    logger.error("No exception in risky code")


    
#FileNotFoundError
try:
    logger.error(open('happy.txt').read())
except Exception as err:
    logger.error(err)
else:
    logger.error("No exception in risky code")


#IndexError
list=[3,5,7,8]
try:
    logger.error(list[10])
except Exception as err:
    logger.error(err)
else:
    logger.error("No exception in risky code")
   