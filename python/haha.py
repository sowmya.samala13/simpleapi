"""import os,sys,stat
import shutil
from os import *

source_path=r"C:\Users\ssamala\Desktop\python\dir"
destination_path=r"C:\Users\ssamala\Desktop\samya"
folder=["py","jpg","txt","png"]
emptylist=[]
for i in folder:
    path=os.path.join(destination_path,i)
    emptylist.append(path)
    os.makedirs(path)
for i in os.listdir(source_path):
    if i.endswith('.py'):                                 
        path=os.path.join(source_path,i)
        shutil.copy2(path,emptylist[0])

    elif i.endswith('.jpg'):
        path=os.path.join(source_path,i)
        shutil.copy2(path,emptylist[1])

    elif i.endswith('.txt'):
        path=os.path.join(source_path,i)
        shutil.copy2(path,emptylist[2])

    elif i.endswith('.png'):
        path=os.path.join(source_path,i)
        shutil.copy2(path,emptylist[3])
             """
import aiohttp
import asyncio
from bs4 import BeautifulSoup
from aiofile import async_open
import threading
from time import perf_counter

async def get_image_urls(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            html = await response.text()

    soup = BeautifulSoup(html, 'html.parser')
    image_urls = []
    for img in soup.find_all("img"):
        try:
            if img["class"][0].startswith("wp-image-"):
                img_url = img["src"].split("?")[0]
                image_urls.append(img_url)
        except KeyError as exc:
            pass
    return image_urls


async def download_image(url, filename):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            async with async_open(filename, 'wb+') as afp:
                async for chunk in response.content.iter_chunked(16*1024):
                    await afp.write(chunk)
    thread = str(threading.current_thread())
    print(f"{filename} downloaded successfully from {url} on {thread}")


async def main():
    image_urls = await get_image_urls(
        "https://triponzy.com/blog/most-beautiful-tourist-places-in-india/")

    print(image_urls)

    tasks = []

    for url in image_urls:
        task = asyncio.create_task(download_image(url, url.split("/")[-1]))
        tasks.append(task)

    for task in tasks:
        await task


if __name__ == "__main__":
    start_time = perf_counter()
    asyncio.run(main())
    end_time = perf_counter()
    time_lapsed = (end_time - start_time)
    print(f"finished executing in {time_lapsed}")
         

         
   

  
