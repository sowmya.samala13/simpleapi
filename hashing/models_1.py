from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import AsyncSession
import asyncio
from asyncio import current_task
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.asyncio import async_scoped_session
from sqlalchemy.ext.asyncio import AsyncSession, AsyncEngine
from sqlalchemy import Column, Integer, String, Index
from sqlalchemy import select
from pydantic import BaseModel
from sqlalchemy.orm import validates
from urllib.parse import urlparse
import re
import os
import hashlib



url_validator = re.compile(
    r'^(?:http|ftp)s?://'  # http:// or https://
    # domain...
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
    r'localhost|'  # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)


#username_validator = re.compile(r"\A[A-Z0-9_.]{5,128}\Z", re.IGNORECASE)
password_validator = re.compile(r"\A[A-Z0-9_.]{5,128}\Z", re.IGNORECASE)

Base = declarative_base()


engine: AsyncEngine = create_async_engine(
    "sqlite+aiosqlite:///main.db", echo=False,
)

async_session_factory = sessionmaker(
    engine, expire_on_commit=False, class_=AsyncSession
)

DBSession = async_scoped_session(
    async_session_factory, scopefunc=current_task
)


class Items(Base):
    __tablename__ = "items"
    id: int = Column(Integer, primary_key=True)
    item_name: str = Column(String(128))
    item_description: str = Column(String(512))
    item_image_url: str = Column(String(2048))

    def __repr__(self):
        return f"Users(id = {self.id}, item_name = '{self.item_name}',\
            item_description = '{self.item_description}', item_image_url='{self.item_image_url}')"

    async def save(self, session: AsyncSession):
        session.add(self)
        await session.commit()

    @validates("item_image_url")
    def validate_image_url(self, key, url):
        if url_validator.fullmatch(url):
            return url
        else:
            raise ValueError(f"invalid url {url}")


class Users(Base):
    __tablename__ = "users"
    id: int = Column("id", Integer, primary_key=True)
    username: str = Column("username", String(128), unique=True, nullable=False)
    hash_algorithm: str = Column("hash_algorithm", String(256))
    no_of_iterations: int = Column("no_of_iterations", Integer)
    salt: str = Column("salt", String(64))
    hashed_password: str = Column("hashed_password", String(128))
    email: str = Column("email", String(512))


    def hash_password(self, password: str):
        self.salt = os.urandom(64).hex()
        self.hash_algorithm = "pbkdf2-hmac-sha512"
        self.no_of_iterations = 100000
        self.hashed_password = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations).hex()


    def authenticate(self, password: str):
        if self.hash_algorithm == "pbkdf2-hmac-sha512":
            new_hash = hashlib.pbkdf2_hmac(self.hash_algorithm.split("-")[-1], password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations)
            if new_hash == self.hashed_password:
                return True
            else:
                return False


    @validates("username")
    def validate_username(self, key, username):
        if username_validator.fullmatch(username):
            return username
        else:
            raise ValueError


async def main():
    async with engine.begin() as connection:
        await connection.run_sync(Base.metadata.drop_all)
        await connection.run_sync(Base.metadata.create_all)

    info = {
        "item_name": "vivo",
        "item_description": "China piece",
        "item_image_url": "https://localhost/vivo12"
    }

    mobile = Items(item_name="vivo", item_description="China piece",
                  item_image_url="https://localhost/vivo12")
    mobile2 = Items(item_name="iphone", item_description="Apple iphone 6s",
                   item_image_url="https://localhost/iphone6s")
    mobile3 = Items(item_name="redmi", item_description="redmi phone",
                   item_image_url="https://localhost/redmi78")

    user1 = Users(username="helloworld", email="something@something.com")
    user1.hash_password("HelloWorld@12345")
    

    async with DBSession() as session:
        async with session.begin():
            session.add(mobile)
            session.add(mobile2)
            session.add(mobile3)
            session.add(user1)
        await session.commit()

        sql_stmt = select(Items)
        print(sql_stmt)
        results = await session.stream_scalars(sql_stmt)

        for row in await results.all():
            print(row)
        
        sql_stmt = select(Users).where(Users.username==user1.username)
        print(sql_stmt)
        results = await session.stream_scalars(sql_stmt)
        user =  await results.first()
        if user.authenticate("HelloWorld@12345"):
            print("user authenticated!")

    await engine.dispose()


if __name__ == "__main__":
    asyncio.run(main())
