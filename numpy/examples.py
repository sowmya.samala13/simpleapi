from cgi import print_directory
import numpy as np
"""
a = np.array([1, 2, 3, 4, 5, 6])
print(a)


a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
print(a[2])

# Create an empty array with 2 elements
a = np.zeros(2)
print(a)

#array with a range of elements
a = np.arange(4)
print(a)

a = np.arange(2, 9, 2)
print(a)

# np.linspace() to create an array with values that are spaced linearly in a specified interval:
a= np.linspace(0, 10, num=5)
print(a)

# using the dtype keyword
x = np.ones(2, dtype=np.int64)
print(x)

#Sorting an element by using np.sort()
arr = np.array([2, 1, 5, 3, 7, 4, 6, 8])
x = np.sort(arr)
print(x)

# argsort, which is an indirect sort along a specified axis,
# lexsort, which is an indirect stable sort on multiple keys,
# searchsorted, which will find elements in a sorted array, and
# partition, which is a partial sort.


#concatenate 2 arrays by using np.concatenate()
a = np.array([1, 2, 3, 4])
b = np.array([5, 6, 7, 8])
x = np.concatenate((a, b))
print(x)

x = np.array([[1, 2], [3, 4]])
y = np.array([[5, 6]])
x = np.concatenate((x, y), axis=0)
print(x)

#shape and size of an array
array_example = np.array([[[0, 1, 2, 3],
                           [4, 5, 6, 7]],

                          [[0, 1, 2, 3],
                           [4, 5, 6, 7]],

                          [[0 ,1 ,2, 3],
                           [4, 5, 6, 7]]])
x = array_example.ndim #ndarray.ndim will tell you the number of axes, or dimensions, of the array
y = array_example.size
z = array_example.shape
print(x) 
print(y) 
print(z) 

#reshape an array 
#Using arr.reshape() will give a new shape to an array without changing the data
a = np.arange(6)
print(a)
b = a.reshape(3, 2)
print(b)
c = a.reshape(2,3)
print(c)
d = np.reshape(a, newshape=(1, 6), order='C')
print(d)

#convert a 1D array into a 2D array (how to add a new axis to an array)
#You can use np.newaxis and np.expand_dims to increase the dimensions of your existing array.
a = np.array([1, 2, 3, 4, 5, 6])
output = a.shape
print(output)

a2 = a[np.newaxis, :]
output = a2.shape
print(output)
#row vector
row_vector = a[np.newaxis, :]
output = row_vector.shape
print(output)
#column vector
col_vector = a[:, np.newaxis]
output = col_vector.shape
print(output)

b = np.expand_dims(a, axis=1)
output = b.shape
print(output)

c = np.expand_dims(a, axis=0)
output = c.shape
print(output)

#Indexing and slicing
data = np.array([1, 2, 3])
print(data[1])
print(data[0:2])
print(data[1:])
print(data[2:3])
print(data[-2:])

#returns values less than input
a = np.array([[1 , 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
print(a[a<5])
five_up = (a >= 5)
print(a[five_up])
#elements that are divisible by 2
divisible_by_2 = a[a%2==0]
print(divisible_by_2)
#using & operation
c = a[(a > 2) & (a < 11)]
print(c)
#retuns boolean values
five_up = (a > 5) | (a == 5)
print(five_up)

#np.nonzero() to select elements or indices from an array.
a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
b = np.nonzero(a < 5)
print(b)

list_of_coordinates= list(zip(b[0], b[1]))
for coord in list_of_coordinates:
    print(coord)
print(a[b])
#given number is present or not
not_there = np.nonzero(a == 42)
print(not_there)

#How to create an array from existing data
a = np.array([1,  2,  3,  4,  5,  6,  7,  8,  9, 10])
a1 = np.array([[1, 1],
               [2, 2]])

a2 = np.array([[3, 3],
               [4, 4]])
output = np.vstack((a1, a2))  #stack them vertically with vstack
print(output)    

#stack them horizontally with hstack
output = np.hstack((a1, a2))  
print(output) 
# arange and reshape at a time
x = np.arange(1, 25).reshape(2, 12)
print(x)
output = np.hsplit(x,3)
print(output)
#If you wanted to split your array after the third and fourth column
output = np.hsplit(x, (3, 4))
print(output)

#Basic array operations
data = np.array([1, 2])
ones = np.ones(2, dtype=int)
print(data + ones)
print(data - ones)
print(data * ones)
print(data / ones)

#sum of array
a = np.array([1, 2, 3, 4])
print(a.sum())

#To add the rows or the columns in a 2D array, you would specify the axis.
b = np.array([[1, 1], [2, 2]])
print(b.sum(axis=0))
print(b.sum(axis=1)) # 1 + 1 =2 nd 2 + 2 = 4 soo ouput is [2,4]


#Broadcasting
data = np.array([1.0, 2.0])
print(data * 1.6)

#More useful array operations min(),max(),sum()
data = np.array([1,2,3,4,5,6])
output = data.max()
print(output)
output = data.min()
print(output)
output = data.sum()
print(output)

#Creating matrices
data = np.array([[1, 2], [3, 4], [5, 6]])
print(data)
print(data[1:3])
print(data[0:1])
print(data[0:2, 0])
print(data.max(axis=0)) #means columns
print(data.max(axis=1)) # means rows

data = np.array([[1, 2], [3, 4], [5, 6]])
ones_row = np.array([[1, 1]])
print(data + ones_row)
print(np.ones((4, 3, 2)))
print(np.ones(3))
print(np.zeros(3))

# the simplest way to generate random numbers
rng = np.random.default_rng(0)
print(rng.random(3))
print(np.ones((3, 2)))
print(np.zeros((3, 2)))
print(rng.random((3, 2)))

#Generating random numbers
#You can generate a 2 x 4 array of random integers between 0 and 4
a = rng.integers(5, size=(2, 4))
print(a)

#How to get unique items and counts
a = np.array([11, 11, 12, 13, 14, 15, 16, 17, 12, 13, 11, 14, 18, 19, 20])
unique_values = np.unique(a)
print(unique_values)
unique_values, occurrence_count = np.unique(a, return_counts=True)
print(occurrence_count)
#2d array
a_2d = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [1, 2, 3, 4]])
unique_values = np.unique(a_2d)
print(unique_values)
#using axis
unique_rows = np.unique(a_2d, axis=0)
print(unique_rows)
print(occurrence_count)
print(unique_rows)

#Transposing and reshaping a matrix

# a = (np.arange(1000).reshape(10,10))
# print(a)

a = np.ones((2,2,2,3))
print(a)
a = np.ones((2,4,3))

a.max(axis=0)
print(a)"""

a = np.array([[1,2,3,4], [5,6,7,8], [9,2,3,4]]) 
b = a.min(axis=0)     
print(b)             