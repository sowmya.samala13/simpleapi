from fastapi import FastAPI, Response, Cookie
from typing import Optional
from pydantic import BaseModel
import json
import os
import hashlib
import re
from lib2to3.pgen2 import token

app = FastAPI()

posts = []
users = []

data = { "posts": [], "users": [] }


class Post(BaseModel):
    id: str
    description: str
    content: str
    author: str


class User(BaseModel):
    username: str
    password: str
    email: str | None

username_validator = re.compile(r"\A[A-Z0-9_.]{5,128}\Z", re.IGNORECASE)

password_validator=re.compile(r"\A[A-Z0-9@$!%*#&_.]{6,20}\Z",re.IGNORECASE)

email_validator=re.compile(r"\A[A-Z0-9@_.]{8,}",re.IGNORECASE)

class Users:
    def __init__(self, username: str = None, email: str = None, salt: str = None, hashed_password: str = None, token: str = None):
        self.username = username
        self.email = email
        self.salt = salt
        self.hashed_password = hashed_password
        self.token = token
    
    def gen_hash(self, password: str):
        """
        Method which generates a salt and hashes the passwords and stores them in the Uers object instance.
        """
        self.salt = os.urandom(64).hex()
        hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), 100000)
        self.hashed_password = hash.hex()
    
    def dict(self):
        return {
            "username": self.username,
            "email": self.email,
            "salt": self.salt,
            "hashed_password": self.hashed_password,
            "token": self.token
        }

    def from_dict(self, dict_obj):
        self.username = dict_obj["username"]
        self.email =  dict_obj["email"]
        self.salt = dict_obj["salt"]
        self.hashed_password = dict_obj["hashed_password"]
        self.token = dict_obj["token"]

    
    def verify(self, password: str)->bool:
        new_hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), 100000)
        if new_hash.hex() == self.hashed_password:
            self.token = os.urandom(64).hex()
            return True
        else:
            return False
    

    def validate_username(self, key, username):
        if username_validator.fullmatch(username):
            return username
        else:
            raise ValueError(f"invalid username {username}")


    def validate_password(self, key, password):
        if password_validator.fullmatch(password):
            return password
        else:
            raise ValueError(f"invalid password {password}")


    def validate_email(self, key, email):
        if email_validator.fullmatch(email):
            return email
        else:
            raise ValueError(f"invalid email {email}")

    @classmethod
    def load_from_dict(cls, dict_obj):
        return Users(
            username = dict_obj["username"],
            email = dict_obj["email"],
            salt = dict_obj["salt"],
            hashed_password= dict_obj["hashed_password"],
            token = dict_obj.get("token")
        )

class Posts:
    def _init_(self, description: str = None, content: Optional[str] =None,author: str = None):
        self.description=description
        self.content=content
        self.author=author
    
    def dict(self):
        description=self.description
        content=self.content
        author=self.author

    def from_dict(self, dict_obj):
        self.description = dict_obj["description"]
        self.content =  dict_obj["content"]
        self.author=dict_obj["author"]
    

def login_check(login_token):
            for user in users:
                if user.token==login_token:
                    return True
                else:
                    return {
                        "message":"login required.login or signup"
                    }
                        
   

@app.get("/")
def hello(login_token: str | None = Cookie(None)): 
    return {
        "message": "Visit '/docs' ",
        "login_token": login_token
    }


@app.post("/post/add")
def add_post(post: Post):
    data.append(post)
    return {
        "message": "Successfully added post to database",
        "post_added": {
            "id": post.id,
            "description": post.description,
            "content": post.content
        }
    }


@app.get("/posts")
def get_all_posts():
    return {
        "results": data
    }



@app.delete("/posts/delete/{post_id}")
def delete_post(post_id: str):
    for entry in data:
        if entry["id"] == post_id:
            data.remove(entry)
            return {
                "message": "Post removed successfully",
                "post": entry
            }
    return {
        "message": "post not found or invalid post_id"
    }


@app.post("/signup")
def signup(user: User):
    for entry in users:
        if entry.username == user.username:
            return {
                "status_code": "0",
                "message": f"User with {user.username} already exists!"
            }   
    new_user = Users(username=user.username, email=user.email)
    new_user.gen_hash(user.password)
    users.append(new_user)
    return {
        "status_code" : "1",
        "message": "user added successfully" 
    }


@app.post("/login")
def login(user: User, response: Response):
    for entry in users:
        if entry.username == user.username:
            if entry.verify(user.password):
                response.set_cookie(key="login_token", value=entry.token)
                return {
                    "message": "login successful! user authenticated!"
                }
            else:
                return {
                    "message": "wrong password. user verification failed"
                }
    return {
        "message": f"user with {user.username} not found in database"
    }

@app.post("/logout")
def logout(user:User,response:Response):
    for entry in users:
        if entry.username==user.username:
            response.delete_cookie(key="login_token")
            return{
                "message":"log out success"
            }
    return {
        "message": f"user with {user.username} not found in database"
    }
    

@app.on_event("startup")
def load_data():
    print("Starting Up")
    file_to_load = "data.json"
    if os.path.exists(file_to_load):
        with open(file_to_load, "r") as file:
            global data, users, posts
            data = json.load(file)
            for user in data["users"]:
                users.append(Users.load_from_dict(user))
            posts = data["posts"]


@app.on_event("shutdown")
def save_data():
    print("Shutting Down")
    with open("data.json", "w") as file:
        users_list = []
        for user in users:
            users_list.append(user.dict())
        posts_list = []
        for post in posts:
            posts_list.append(post.dict())
        data["posts"] = posts_list
        data["users"] = users_list
        json.dump(data, file, indent=4)

        #https://fastapi.tiangolo.com/


"""from fastapi import FastAPI
from fastapi.responses import FileResponse

some_file_path = "large-video-file.mp4"
app = FastAPI()


@app.get("/")
async def main():
    return FileResponse(some_file_path)"""