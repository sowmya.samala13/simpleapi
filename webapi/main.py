from fastapi import FastAPI
from pydantic import BaseModel
import os
import json

app = FastAPI()

data = []

class post(BaseModel):
    id:str
    desc: str
    content:str

@app .get("/")   
def fun(a:str,b:int | None = None):
    return{"a":a,"b":b}

@app.post("/post/add")  
def add_post(post:post):
    data.append(post.dict())
    return {
        "message" :"successfully added to post to database",
        "post_added" : {
            "id": post.id,
            "desc":post.desc,
            "content": post.content
        }
    }

@app.get("/posts")
def get_all_posts():
    return {
        "results":data
    }



@app.on_event("startup")
def load_data():
    file_to_load = "data.json"
    if os.path.exists(file_to_load):
        with open(file_to_load,"r") as file:
            global data
            data = json.load(file)

@app.on_event("shutdown")            
def save_data():
    with open("data.json","w") as file:
        json.dump(data,file)

