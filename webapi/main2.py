from fastapi import FastAPI
from pydantic import BaseModel
import os
import json
import hashlib

app = FastAPI()

data = []

class EmpDetails(BaseModel):
    emp_id:str
    emp_desc: str
    emp_salary:int

class Usersignin(BaseModel):
    username:str
    email:str
    password: str

class Config:
        schema_extra = {
            "message": {
                "username": "Abdulazeez Abdulazeez Adeshina",
                "email": "abdulazeez@x.com",
                "password": "weakpassword"
            }
        }

class UserLogin(BaseModel):
    email:str
    password:str

class Config:
        schema_extra = {
            "message": {
                "email": "abdulazeez@x.com",
                "password": "weakpassword"
            }
        }

@app.get("/")   
def Hello(a:str,b:int | None = None):
    return{"a":a,"b":b}

"""def hash_password(self, password: str):
        self.salt = os.urandom(64).hex()
        self.hash_algorithm = "pbkdf2-hmac-sha512"
        self.no_of_iterations = 100000
        self.hashed_password = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations).hex()


def authenticate(self, password: str):
        if self.hash_algorithm == "pbkdf2-hmac-sha512":
            new_hash = hashlib.pbkdf2_hmac(self.hash_algorithm.split("-")[-1], password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations).hex()
            if new_hash == self.hashed_password:
                return True
            else:
                return False"""    

def hash_password(self):

        self.hashed_password = hashlib.pbkdf2_hmac("sha512",self.password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations).hex()

        return {
            "username":self.username,
            "hashed_password":self.hashed_password,
            "email":self.email,
            "salt":self.salt
            
        }

def authenticate(self, password,salt,hashed_password):
        new_hash = hashlib.pbkdf2_hmac(self.hash_algorithm.split("-")[-1], password.encode("utf-8"), salt.encode("utf-8"), self.no_of_iterations).hex()
        if new_hash == hashed_password:
            return True
        else:
            return False
            
@app.post("/empdetails")  
def add_post(emp:EmpDetails):
    data.append(emp.dict())
    return {
        "message" :"successfully added empdetails to database",
        "empdetails_added" : {
             "emp_id": emp.emp_id,
             "emp_desc":emp.emp_desc,
             "emp_salary": emp.emp_salary
        }
    }  


@app.post("/Usersignin")  
def add_post(signin:Usersignin):
    data.append(signin.dict())
    return {
        "message" :"successfull signup",
        "Users_added" : {
             "username": signin.username,
             "password":signin.password,
             "email": signin.email
        }
    }  

@app.post("/UserLogin")  
def add_post(Login:UserLogin):
    data.append(Login.dict())
    return {
        "message" :"successfully logined ",
        "Users_added" : {
            "email": Login.email,
            "password":Login.password
          
        }
    }  

@app.get("/posts")
def get_all_posts():
    return {
        "results":data
    }    

@app.get("/Usersignin")
def get_all_Usersignin():
    return {
        "results":data
    }        

@app.get("/UserLogin")
def get_all_UserLogin():
    return {
        "results":data
    }      


@app.delete ("/empdetails/ {emp_id}")
def delete_emp_id(emp_id: str):    
    for entry in data:
        if entry ["emp_id"] == emp_id:
            data.remove(entry)
        return {
            "message":"successfully deleted empdetails from database",
            "emp_id.deleted":
            {
                "emp_id":data
            }
        }    


@app.on_event("startup")
def load_data():
    file_to_load = "data.json"
    if os.path.exists(file_to_load):
        with open(file_to_load,"r") as file:
            global data
            data = json.load(file)  

@app.on_event("shutdown")            
def save_data():
    with open("data.json","w") as file:
        json.dump(data,file)



