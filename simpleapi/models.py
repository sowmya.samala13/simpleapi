from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import validates,sessionmaker
from sqlalchemy import create_engine
from sqlalchemy import Column,String,Integer,Float
import hashlib
import os,json
import requests as r
import re
from time import sleep

url_validator = re.compile(
    r'^(?:http|ftp)s?://'  # http:// or https://
    # domain...
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
    r'localhost|'  # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)


password_validator="^(?=.[a-z])(?=.[A-Z])(?=.\d)(?=.[@$!%#?&])[A-Za-z\d@$!#%?&]{6,20}$"
username_validator = re.compile(r"\A[A-Z0-9_.]{5,128}\Z", re.IGNORECASE)
email_validator='^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'


engine=create_engine("sqlite+pysqlite:///main.db")

Session=sessionmaker(bind=engine)
session=Session()

Base=declarative_base()


class Users(Base):
    __tablename__= "users"
    id: int = Column("id", Integer, primary_key=True)
    username: str = Column("username", String(128), unique=True, nullable=False)
    hash_algorithm: str = Column("hash_algorithm", String(256))
    no_of_iterations: int = Column("no_of_iterations", Integer)
    salt: str = Column("salt", String(64))
    hashed_password: str = Column("hashed_password", String(128))
    email: str = Column("email", String(512))
    token: str = Column("token",String(128))



    def hash_password(self, password: str):
        self.salt = os.urandom(64).hex()
        self.hash_algorithm = "pbkdf2-hmac-sha512"
        self.no_of_iterations = 100000
        self.hashed_password = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations).hex()


    def authenticate(self, password: str):
        if self.hash_algorithm == "pbkdf2-hmac-sha512":
            new_hash = hashlib.pbkdf2_hmac(self.hash_algorithm.split("-")[-1], password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations).hex()
            if new_hash == self.hashed_password:
                self.token=os.urandom(64).hex()
                return True
            else:
                return False
    @validates("username")
    def validate_username(self, key, username):
        if username_validator.fullmatch(username):
            return username
        else:
            raise ValueError

    @validates("email_id")
    def validate_email(self,email):
        if email.fullmatch(email_validator):
            return email
        else:
            raise ValueError(f"invalid password {email}")

    @validates("password")
    def validate_password(self,password):
        if password.fullmatch(password_validator):
            return password
        else:
            raise ValueError(f"invalid password {password}")


class Posts(Base):
    __tablename__= "items"
    id: int = Column(Integer, primary_key=True)
    description: str = Column(String(512))
    image_url: str = Column(String(2048))
    author:str=Column(String(2048))



class Coins():
    def __init__(self,ids,curr):   
        self.ids=ids
        self.cuuriences=curr
        self.price=0
        self.price_change_in_24h=0
        self.price_change_in_1h=0
        self.price_change_in_7d=0
        
    def price_creator(self):
        resp_1=r.get('https://api.coingecko.com/api/v3/coins',params={'id':self.ids})
        coins_data=resp_1.json()                   
        self.price=coins_data[0]['market_data']['current_price'][self.cuuriences]
        self.price_change_in_24h=coins_data[0]['market_data']["price_change_24h_in_currency"][self.cuuriences]
        self.price_change_in_1h=coins_data[0]['market_data']["price_change_percentage_1h_in_currency"][self.cuuriences]
        self.price_change_in_7d=coins_data[0]['market_data']["price_change_percentage_7d_in_currency"][self.cuuriences]
            


 








Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)





































"""import os
import re
import hashlib
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import column, create_engine
from sqlalchemy import String,Column, Integer, ForeignKey
from sqlalchemy.future import Engine
from sqlalchemy.orm import validates
from sqlalchemy.orm import Session

Base=declarative_base()

engine: Engine= create_engine("sqlite+pysqlite:///main.db")
session=Session(engine)

username_validator = re.compile(r"\A[A-Z0-9_.]{5,128}\Z", re.IGNORECASE)
email_validator = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')

class Posts(Base):
    __tablename__="posts"
    id : int =Column(Integer(),primary_key=True)
    description: str = Column(String(540))
    content : str = Column(String(600))
    author : str = Column(String(64),ForeignKey("users.username"))


class Users(Base):
    __tablename__ = "users"
    username: str=Column(String(64),primary_key=True)
    email : str = Column(String(256),unique=True,nullable = False)
    salt : str = Column(String(128))
    hashed_password : str = Column(String(128))
    token : str = Column(String(128))


    @validates("username")
    def validate_username(self, key, username):
        if username_validator.fullmatch(username):
            return username
        else:
            raise ValueError("invalid username")
    
    @validates("email")
    def validate_username(self,key,email):
        if email_validator.fullmatch(email):
            return email
        else:
            raise ValueError("invalid email")

    def gen_hash(self, password: str):
        self.salt = os.urandom(64).hex()
        hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), 100000)
        self.hashed_password = hash.hex()

    
    def authenticate(self, password: str,salt: str,check_password) -> bool:
        # if self.hash_algorithm == "pbkdf2-hmac-sha512":
            new_hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), salt.encode("utf-8"), 100000).hex()
            if new_hash ==check_password:
                return True
            else:
                return False

Base.metadata.create_all(engine)



"""