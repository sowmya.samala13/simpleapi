from fastapi import FastAPI,Response, Cookie,Depends
from models import session, Users,engine,Base,Posts,Coins
from sqlalchemy import Integer, select
from pydantic import BaseModel
from time import sleep
import os

app = FastAPI()

class User(BaseModel):
    username: str
    email: str
    password: str

class Login(BaseModel):
    username: str
    email: str
    password: str

class Post(BaseModel):
    image_url: str
    description: str
    author: str

class Data(BaseModel):
    ids: str
    curr: str

@app.get("/")
def hello(login_token: str | None = Cookie(None)):
    return {
        "message": "Visit '/docs' ",
        "login_token": login_token
    }

@app.post("/user/signup")
def add_user(user: User):        
        user_to_add = Users(username=user.username,email=user.email)
        user_to_add.hash_password(user.password)
        session.add(user_to_add)
        session.commit()
        return {
            "message": "user added",
            "added_user": user.username,
            "email_added":user.email
        }
@app.post("/user/login")
def login_user(login_user : Login,response: Response):
    user_logged=session.query(Users).filter(Users.username==login_user.username).first()
    if user_logged.token == None:
        if user_logged.authenticate(login_user.password):
            response.set_cookie(key="login_token", value=user_logged.token)
            session.commit()
            return {'message':'login succcesful'}
        else:
            return {'message':'login failed password wrong'}
    else:
        return {'message':'already logged in'}


@app.get("/user/logout/{username}")
def logout_user(username:str,response: Response,login_token: str | None = Cookie(None)):
    user_logged_1=session.query(Users).filter(Users.username==username).first()
    if user_logged_1.token != None:
        response.delete_cookie(key="login_token")
        user_logged_1.token = None
        session.commit()
        return {'message':'logged out succesfully'}
    else:
        return {'message':user_logged_1.token}


@app.post("/posts/add")
def add_posts(posts : Post):
    post_to_add = Posts(description=posts.description,image_url=posts.image_url,author=posts.author)
    session.add(post_to_add)
    session.commit()
    return {'message':'post added succesfully'}

@app.get('/posts/{getall}')
def get_all_posts():
    user_logged_2=session.query(Posts).first()
    return {'message':user_logged_2}

@app.post('/coins/data')
def get_price_live(data : Data,login_token: str | None = Cookie(None)):
    if login_token != None:
        user_logged=session.query(Users).filter(Users.token==login_token).first()
        if user_logged!=None:
            obj=Coins(data.ids,data.curr)
            obj.price_creator()
            return {'ids':obj.ids,
            'cuuriences':obj.cuuriences,
            'price':obj.price,
            'price_change_in_24h':obj.price_change_in_24h,
            'price_change_in_1h':obj.price_change_in_1h,
            'price_change_in_7d':obj.price_change_in_7d
            }
        else:
            return {"message":"Invalid User"}
    else:
        return {"message":"user_logged.token"}
















"""from fastapi import FastAPI
from pydantic import BaseModel
from models import Users,Posts,session
import os

app=FastAPI()

class users(BaseModel):
    username: str
    email: str

class posts(BaseModel):
    id : int
    description : str
    content : str


@app.get("/")
def root():
    return {
        #"message":"http://127.0.0.1:8000/.docs"
        "message":"haihello"
    }
@app.post("/users/signup")
def user_add(user_to_add:users,password: str):
    db_user=session.query(Users).filter(Users.username == user_to_add.username)
    try:
        if db_user!=None:
            user=Users(**user_to_add.dict())
            user.gen_hash(password)
            session.add(user)
            session.commit()
            return{
                "username":user_to_add.username,
                "email":user_to_add.email,
                }
        else:
            return{
                "message":f"username {user_to_add.username} already taken"
            }
    except ValueError as err:
        return {
            "message":str(err)
        }


@app.post("/user/login")
def user_login(user_to_login:users,password:str):
    db_user=session.query(Users).filter(Users.username == user_to_login.username).first()
    if db_user==None:
        return {
            "message":"Login failed"
        }
    elif db_user.token==None:
        user=Users(**user_to_login.dict())
        if user.authenticate(password,db_user.salt,db_user.hashed_password):
            db_user.token = os.urandom(64).hex()
            session.commit()
            return {
                "message":"Login success"
            }
    else:
        return{
            "message": f"user {user_to_login.username} already loggedin"

        }
        
@app.post("/user/logout")
def user_logout(user_to_logout: users):
    user_logout=session.query(Users).filter( Users.username==user_to_logout.username).first()
    if user_logout.token:
        user_logout.token=None
        session.commit()
        return{
            "message":"Logout success"
        }
    else:
        return{
            "message":"Signup or Login required"
        }


@app.post("/user/post")
def add_post(post: posts):
    post=Posts(**post.dict())
    session.add(post)
    session.commit()
    return{
        "message":"post added succesfully"
    }"""