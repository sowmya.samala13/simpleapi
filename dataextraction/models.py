from sqlalchemy import Column, Integer, create_engine,Column,Integer,String,create_engine,Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import requests as r
from time import sleep
import json
import string

engine = create_engine("sqlite+pysqlite:///main.db",echo=False)

Session = sessionmaker(bind=engine)
session = Session()

Base=declarative_base()

class Coins(Base):
    __tablename__='coins'
    id=Column(Integer,primary_key=True)
    ids=Column(String(50))
    curiences=Column(String(50))
    price_change=Column(Integer)
    price_change_in_24h=Column(Float)
    price_change_in_7d=Column(Float)
    price_change_in_1h=Column(Float)
    

    def present_price(self):
        resp_1=r.get('https://api.coingecko.com/api/v3/coins',params={'id':self.ids})
        self.coins_data=resp_1.json()
        self.price_change=self.coins_data[0]['market_data']['current_price'][self.curiences]
    

    def price_change_24h(self):               
        self.price_change_in_24h=self.coins_data[0]['market_data']["price_change_24h_in_currency"][self.curiences]
    
    
    def price_change_1h(self):
        self.price_change_in_1h=self.coins_data[0]['market_data']["price_change_percentage_1h_in_currency"][self.curiences]

    def price_change_7d(self):
        self.price_change_in_7d=self.coins_data[0]['market_data']["price_change_percentage_7d_in_currency"][self.curiences]



Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)

















"""from sqlalchemy import Column, Integer, create_engine,Column,Integer,String,create_engine,Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import requests as r
from time import sleep
import json
import string

engine = create_engine("sqlite+pysqlite:///main.db",echo=False)

Session = sessionmaker(bind=engine)
session = Session()

Base=declarative_base()

class Coins(Base):
    __tablename__='coins'
    id=Column(Integer,primary_key=True)
    ids=Column(String(50))
    curiences=Column(String(50))
    price_change=Column(Integer)
    price_change_in_24h=Column(Float)
    price_change_in_7d=Column(Float)
    price_change_in_1h=Column(Float)
    

    def present_price(self):
        resp_1=r.get('https://api.coingecko.com/api/v3/coins',params={'id':self.ids})
        coins_data=resp_1.json()
        with open('coins.json','w') as object:
            json.dump(coins_data,object,indent=4)
            
        with open('coins.json','r') as file:
            json_data=file.read()
            python_data=json.loads(json_data)               
            self.price_change=python_data[0]['market_data']['current_price'][self.curiences]
            # self.price_change_in_24h=python_data[0]['market_data']["price_change_24h_in_currency"][self.curiences]
            # self.price_change_in_7d=python_data[0]['market_data']["price_change_percentage_7d_in_currency"][self.curiences]
            # self.price_change_in_1h=python_data[0]['market_data']["price_change_percentage_1h_in_currency"][self.curiences]

    def price_change_24h(self):
        with open('coins.json','r') as file:
            json_data=file.read()
            python_data=json.loads(json_data)               
            self.price_change_in_24h=python_data[0]['market_data']["price_change_24h_in_currency"][self.curiences]
    

    def price_change_7d(self):
          with open('coins.json','r') as file:
              json_data=file.read()
              python_data=json.loads(json_data)               
              self.price_change_in_7d=python_data[0]['market_data']["price_change_percentage_7d_in_currency"][self.curiences]


    def price_change_1h(self):
        with open('coins.json','r') as file:
            json_data=file.read()
            python_data=json.loads(json_data)               
            self.price_change_in_1h=python_data[0]['market_data']["price_change_percentage_1h_in_currency"][self.curiences]

    
#Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)"""








































"""import os
import re
import requests
import requests as s
import json
import hashlib
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import column, create_engine
from sqlalchemy import String,Column, Integer, ForeignKey
from sqlalchemy.future import Engine
from sqlalchemy.orm import validates
from sqlalchemy.orm import Session


Base=declarative_base()

engine: Engine= create_engine("sqlite+pysqlite:///main.db")
session=Session(engine)


username_validator = re.compile(r"\A[A-Z0-9_.]{5,128}\Z", re.IGNORECASE)
email_validator = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')


class Posts(Base):
    __tablename__="posts"
    id : int =Column(Integer(),primary_key=True)
    description: str = Column(String(540))
    content : str = Column(String(600))
    author : str = Column(String(64),ForeignKey("users.username"))

class Price(Base):
    __tablename__="Price"
    useranme:str=Column(String(256),primary_key=True)
    id:str=Column(String(600))
    currency:str=Column(String(1024))

class Response():
    def __init__(self,id,currency):
        self.id=id
        self.currency=currency

    def simple_price(self):
        res = requests.get("https://api.coingecko.com/api/v3/simple/price", params={"ids":self.id,"vs_currencies":self.currency})
        return res.json()


class Users(Base):
    __tablename__ = "users"
    username: str=Column(String(64),primary_key=True)
    email : str = Column(String(256),unique=True,nullable = False)
    salt : str = Column(String(128))
    hashed_password : str = Column(String(128))
    token : str = Column(String(128))

   
    def gen_hash(self, password: str):
        self.salt = os.urandom(64).hex()
        hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), 100000)
        self.hashed_password = hash.hex()

    
    def authenticate(self, password: str,salt: str,check_password) -> bool:
        # if self.hash_algorithm == "pbkdf2-hmac-sha512":
            new_hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), salt.encode("utf-8"), 100000).hex()
            if new_hash ==check_password:
                return True
            else:
                return False

Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)



"""